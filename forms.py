from django import forms


class RedisForm(forms.Form):
    key = forms.CharField(max_length=100)
    value = forms.CharField()


class RedisSettings(forms.Form):
    host = forms.CharField()
    port = forms.IntegerField()
    db = forms.IntegerField()
