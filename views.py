from django.shortcuts import render, redirect
import redis
from .forms import *
from django.forms import formset_factory


def add_to_redis_view(request):
    error_log = ''
    RedisFormSet = formset_factory(RedisForm, can_delete=True, extra=1)
    host = request.GET.get('host', '192.168.62.28')
    port = int(request.GET.get('port', '6379'))
    db = int(request.GET.get('db', 1))
    my_redis = redis.StrictRedis(host=host, port=port, db=db)
    config_form = RedisSettings(initial={'host': host, 'port': port, 'db': db})

    if request.method == 'POST':
        redis_initial_data = {key.decode('utf8'): my_redis.get(key).decode('utf8') for key in my_redis.scan_iter()}
        redis_formset = RedisFormSet(request.POST)
        if redis_formset.is_valid():
            delete_forms_from_redis(redis_formset, my_redis)
            update_or_create_forms_in_redis(redis_formset, my_redis, redis_initial_data)
            return redirect(request.path)

    try:
        redis_initial_data = [{'key': key, 'value': my_redis.get(key)} for key in my_redis.scan_iter()]
        redis_formset = RedisFormSet(initial=redis_initial_data)
    except redis.exceptions.InvalidResponse:
        redis_formset = ''
        error_log = 'InvalidResponse Wrong redis database configuration'
    finally:
        return render(request, 'add_to_redis.html', {
            'redis_formset': redis_formset,
            'config': config_form,
            'error_log': error_log
        })


def delete_forms_from_redis(formset, my_redis):
    for obj in formset.deleted_forms:
        my_redis.delete(obj.cleaned_data['key'])


def update_or_create_forms_in_redis(formset, my_redis, redis_initial_data):
    for obj in formset.cleaned_data:
        try:
            if obj.get('key', False) and not redis_initial_data.get(obj['key'], None) == obj['value']:
                my_redis.set(obj['key'], obj['value'])
        except:
            print('error')
